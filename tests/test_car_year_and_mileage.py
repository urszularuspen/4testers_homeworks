from src.car_year_and_mileage import check_if_warranty_is_active


def test_if_for_new_car_with_low_mileage_warranty_is_active():
    assert check_if_warranty_is_active(2020, 30000)


def test_if_for_new_car_with_high_mileage_warranty_is_active():
    assert not check_if_warranty_is_active(2020, 70000)


def test_if_for_old_car_with_low_mileage_warranty_is_active():
    assert not check_if_warranty_is_active(2016, 30000)


def test_if_for_old_car_with_high_mileage_warranty_is_active():
    assert not check_if_warranty_is_active(2016, 120000)
