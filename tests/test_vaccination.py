from src.vaccination import check_if_vaccination_is_needed


def test_cat_age_one_requires_vaccination():
    assert check_if_vaccination_is_needed("cat", 1) == True


def test_dog_age_one_requires_vaccination():
    assert check_if_vaccination_is_needed("dog", 1) == True


def test_dog_age_two_requires_vaccination():
    assert check_if_vaccination_is_needed("dog", 3) == False
