from src.employee_management import generate_dictionary_with_random_data


def test_single_employee_generation_seniority_years_have_correct_value():
    test_employee = generate_dictionary_with_random_data()
    employee_seniority_years = test_employee['seniority_years']
    assert employee_seniority_years >= 0
    assert employee_seniority_years <= 30


def test_single_employee_generated_has_proper_keys():
    test_employee = generate_dictionary_with_random_data()
    assert "email" in test_employee.keys()
    assert "seniority_years" in test_employee.keys()
    assert "female" in test_employee.keys()
