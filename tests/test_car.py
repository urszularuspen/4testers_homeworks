from src.car import Car


def test_car_original_params():
    new_car_1 = Car("Hyundai", "red", 2009)
    assert new_car_1.make == "Hyundai"
    assert new_car_1.colour == "red"
    assert new_car_1.year == 2009


def test_car_mileage_calculation():
    new_car_2 = Car("VW", "blue", 2023)
    new_car_2.drive(30)
    assert new_car_2.mileage == 30


def test_car_age():
    new_car_3 = Car("Toyota", "green", 2000)
    new_car_3_age = new_car_3.get_age()
    assert new_car_3_age >= 0


def test_car_colour():
    new_car_4 = Car("Toyota", "black", 2000)
    new_car_4.repaint("white")
    assert new_car_4.colour == "white"
    assert new_car_4.original_colour == "black"


def test_str():
    new_car_5 = Car("VW", "blue", 2000)
    new_car_5.repaint("silver")
    expected_output = ("My car make is VW.\n"
                       "It was produced in 2000 so it is 24 years old.\n"
                       "The mileage is 0.\n"
                       "It used to be blue but now is silver.")

    assert new_car_5.__str__() == expected_output
