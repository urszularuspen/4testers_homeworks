import random
import string


def generate_random_email():
    domain = "@example.com"
    username_length = random.randint(5, 10)
    username = ''.join(random.choice(string.ascii_lowercase) for _ in range(username_length))
    random_email = username + domain
    return random_email


def generate_dictionary_with_random_data():
    random_email = generate_random_email()
    random_seniority = random.randrange(30)
    random_boolean = random.random() > 0.5
    return {
        "email": random_email,
        "seniority_years": random_seniority,
        "female": random_boolean
    }


def generate_list_of_dictionaries_with_random_data(number_of_dictionaries):
    list_of_dictionaries_with_random_data = []
    for dictionary in range(0, number_of_dictionaries):
        list_of_dictionaries_with_random_data.append(generate_dictionary_with_random_data())
    return list_of_dictionaries_with_random_data


def generate_list_of_emails_of_employees_with_seniority_above_ten_years(employee_dictionaries):
    list_of_employees_with_seniority = []
    for dictionary in employee_dictionaries:
        if dictionary["seniority_years"] >= 10:
            list_of_employees_with_seniority.append(dictionary["email"])
    return list_of_employees_with_seniority


def generate_list_of_female_employees(employee_dictionaries):
    list_of_female_employees = []
    for dictionary in employee_dictionaries:
        if dictionary["female"]:
            list_of_female_employees.append(dictionary)
    return list_of_female_employees


if __name__ == '__main__':
    list_1_with_ten_users = generate_list_of_dictionaries_with_random_data(10)
    print(f"List 1 of ten users with random data: {list_1_with_ten_users}")
    list_1_seniority_above_ten_years = generate_list_of_emails_of_employees_with_seniority_above_ten_years(list_1_with_ten_users)
    print(f"Employees with seniority above ten years: {list_1_seniority_above_ten_years}")
    list_1_female_employees = generate_list_of_female_employees(list_1_with_ten_users)
    print(f"Female employees from list 1: {list_1_female_employees}")

