def get_player_description(data):
    source = data.get("source", "Steam")
    print(f'The player {data["nick"]} is of type {data["type"]} and has {data["exp_points"]} EXP')
    print(f'Additional data {source}')


if __name__ == '__main__':
    player1_details = {
        "nick": "maestro_54",
        "type": "warrior",
        "exp_points": 3000
    }
    get_player_description(player1_details)