import datetime


def check_if_warranty_is_active(year, mileage):
    if (datetime.date.today().year - year) >= 5 or mileage >= 60000:
        return False
    else:
        return True


if __name__ == '__main__':
    print(check_if_warranty_is_active(2020, 30000))
    print(check_if_warranty_is_active(2020, 70000))
    print(check_if_warranty_is_active(2016, 30000))
    print(check_if_warranty_is_active(2016, 120000))
