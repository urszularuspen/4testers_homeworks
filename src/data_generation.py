import random
import datetime

female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
male_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']


def select_first_name(is_female):
    if is_female:
        return random.choice(female_fnames)
    else:
        return random.choice(male_fnames)


def select_lastname():
    return random.choice(surnames)


def select_country():
    return random.choice(countries)


def generate_email(name, lastname):
    return f"{name.lower()}.{lastname.lower()}@example.com"


def generate_age():
    return random.randint(1, 65)


def check_if_adult(age):
    return age >= 18


def calculate_birth_year(age):
    return datetime.date.today().year - age


def generate_user(is_female):
    firstname = select_first_name(is_female)
    lastname = select_lastname()
    age = generate_age()
    return {
        'firstname': firstname,
        'lastname': lastname,
        'email': generate_email(firstname, lastname),
        'age': age,
        'country': select_country(),
        'adult': check_if_adult(age),
        'birth_year': calculate_birth_year(age)
    }


def generate_test_users():
    test_users = []
    for user in range(5):
        test_users.append(generate_user(is_female=True))
    for user in range(5):
        test_users.append(generate_user(is_female=False))
    return test_users


def say_hello(user):
    greeting = f'Hi! I\'m {user["firstname"].upper()} {user["lastname"].upper()}.'
    origin = f'I come from {user["country"].upper()}'
    born = f'and I was born in {user["birth_year"]}.'
    print(f'{greeting} {origin} {born}')


if __name__ == '__main__':
    users = generate_test_users()
    print(users)
    for user in users:
        say_hello(user)
