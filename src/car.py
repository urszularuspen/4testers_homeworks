import datetime


class Car:

    def __init__(self, make, colour, year):
        self.make = make
        self.original_colour = colour
        self.colour = colour
        self.year = year
        self.mileage = 0

    def drive(self, distance):
        self.mileage += distance

    def get_age(self):
        age = datetime.datetime.now().year - self.year
        return age

    def repaint(self, new_colour):
        self.colour = new_colour

    def __str__(self):
        age = self.get_age()
        if age == 1:
            age_text = "1 year old"
        else:
            age_text = f"{age} years old"

        if self.original_colour != self.colour:
            colour_message = f"It used to be {self.original_colour} but now is {self.colour}."
        else:
            colour_message = f"It is {self.colour}."

        return (f"My car make is {self.make}.\n"
                f"It was produced in {self.year} so it is {age_text}.\n"
                f"The mileage is {self.mileage}.\n"
                f"{colour_message}")


if __name__ == '__main__':
    new_car_1 = Car("Hyundai", "red", 2009)
    new_car_2 = Car("VW", "blue", 2023)
    new_car_3 = Car("Toyota", "green", 2000)
    new_car_2.drive(30)
    new_car_2.repaint("silver")
    print(new_car_1)
    print(new_car_2)
    print(new_car_3)
